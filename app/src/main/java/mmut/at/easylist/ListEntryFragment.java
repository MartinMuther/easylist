package mmut.at.easylist;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListEntryFragment extends Fragment {


    public ListEntryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v = inflater.inflate(R.layout.fragment_list_entry, container, false);
         Button btn_savelistentry = (Button) v.findViewById(R.id.btn_savelistentry);


        btn_savelistentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.Container, new OverviewFragment()).addToBackStack("tag").commit();
            }
        });


         return v;





    }

}
